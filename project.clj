(defproject n-back "0.1.0-SNAPSHOT"

  :description "An implementation of the N-back, a task to improve working memory."

  :url "http://gitlab.com/vise890/n-back"

  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :plugins [[lein-codox "0.10.3"]]

  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/test.check "0.10.0-alpha2"]])
