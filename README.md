# n-back

My very own implementation of [N-back] (https://en.wikipedia.org/wiki/N-back), a
task to improve working memory.

## [API Docs](https://vise890.gitlab.io/n-back/)

## Usage

```
;;; FIXME nothing to see here yet
```

## License

Copyright © 2017 Martino Visintin

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
