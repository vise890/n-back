(ns n-back.core
  "Functions for the generation and querying of sequences in the N-back game.


  The terms used throughout are:

  n          : N in N-back. N can be thought of as a 1-based index from the end of xs
  xs         : N-back sequence
  x-back     : element in xs at the n-back position
  p          : probability of next-x being equal to x-back
  candidates : possible choices for x")

(defn rand-unique-el
  "Returns a random element of coll, giving the same probability to each of its unique elements."
  [coll]
  {:pre [(not-empty coll)]}
  (-> coll set vec rand-nth))

(defn contains-x-back?
  "Returns whether the xs has an element at the n-back position (i.e. if it's long enough).

  n  : N in N-back
  xs : N-back sequence so far"
  [n xs]
  (>= (count xs) n))

(defn x-back
  "Returns the element in xs at the n-back position.

  n  : N in N-back
  xs : N-back sequence so far"
  [n xs]
  (nth xs (- (count xs) n)))

(defn ->next-x
  "Generate the next x in a game of N-back.

  candidates : possible choices for x
  n          : N in N-back
  p          : probability of next-x being equal to x-back
  xs         : N-back sequence so far"
  [candidates n p xs]
  {:pre [(not-empty candidates)]}
  (if-not (contains-x-back? n xs)
    (rand-unique-el candidates)
    (if (< (rand) p)
      (x-back n xs)
      (rand-unique-el (disj (set candidates)
                            (x-back n xs))))))

(defn ->xs
  "Returns a lazy (infinite!) N-back sequence.

  candidates : possible choices for x
  n          : N in N-back
  p          : probability of next-x being equal to x-back"
  ([candidates n p]
   (->xs candidates n p (->next-x candidates n p ()) ()))
  ([candidates n p nextx xs]
   (cons nextx
         (lazy-seq (let [nextx (->next-x candidates n p xs)]
                     (->xs candidates n p nextx
                           (cons nextx xs)))))))

(defn x-back=
  "Returns whether x is equal to the element at the n-back position in xs.

  n  : N in N-back
  xs : N-back sequence so far
  x  : the value to test"
  [n xs x]
  (and (contains-x-back? n xs)
       (= (x-back n xs) x)))
