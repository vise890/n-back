(ns n-back.core-test
  (:require [clojure.test :refer :all]
            [clojure.test.check.clojure-test :refer [defspec]]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]
            [n-back.core :as sut]))

(deftest x-back-test

  (testing "contains-x-back? returns whether xs has an element in the x-back position"
    (is (not (sut/contains-x-back? 1 [])))
    (is (sut/contains-x-back? 1 [:a]))
    (is (sut/contains-x-back? 2 [:a :b]))
    (is (sut/contains-x-back? 3 [:a :b :c])))

  (testing "x-back returns the element at the x-back position"
    (is (= :c (sut/x-back 1 [:a :b :c])))
    (is (= :b (sut/x-back 2 [:a :b :c])))
    (is (= :a (sut/x-back 3 [:a :b :c]))))

  (testing "x-back= returns whether the provided x is equal to the element at the x-back position"
    (is (sut/x-back= 1 [:a :b :c] :c))
    (is (sut/x-back= 2 [:a :b :c] :b))
    (is (sut/x-back= 3 [:a :b :c] :a))))

(defspec ->next-x-test
  100

  (prop/for-all
   [candidates (gen/not-empty (gen/set gen/keyword))
    n gen/s-pos-int
    xs (gen/list gen/keyword)]

   (if (sut/contains-x-back? n xs)

     (testing "if xs is long enough to contain an x-back,"
       (let [x-back (sut/x-back n xs)]

         (testing "with p=1, next-x is always equal to x-back"
           (is (= x-back (sut/->next-x candidates n 1 xs))))

         (testing "with p=0,"
           (let [p 0]
             (testing "next-x is never equal to x-back"
               (is (not= x-back (sut/->next-x candidates n p xs))))
             (testing "next-x is always chosen from candidates"
               (is (contains? candidates (sut/->next-x candidates n p xs))))))

         (testing "with any other p, next-x is either equal to x-back or chosen from candidates"
           (let [p      (rand)
                 next-x (sut/->next-x candidates n p xs)]
             (is (or (= x-back next-x) (contains? candidates next-x)))))))

     (testing "if xs is _not_ long enough, next-x is always chosen from candidates"
       (is (contains? candidates (sut/->next-x candidates n 1 xs)))
       (is (contains? candidates (sut/->next-x candidates n 0 xs)))
       (is (contains? candidates (sut/->next-x candidates n (rand) xs)))))))

(defspec ->xs-test
  100

  (prop/for-all
   [candidates (gen/such-that #(<= 2 (count %))
                              (gen/not-empty (gen/set gen/keyword)))
    n gen/s-pos-int
    xs-length gen/pos-int]

   (testing "all the elements are chosen from candidates"
     (let [p  (rand)
           xs (take xs-length (sut/->xs candidates n p))]
       (is (every? candidates xs)
           (str {:n n, :xs xs, :candidates candidates}))))

   (testing "with p=1,"
     (let [p      1.0
           xs     (take xs-length (sut/->xs candidates n p))
           next-x (sut/->next-x candidates n p xs)]
       (testing "either xs is not long enough yet, or next-x is = to x-back"
         (is (or (not (sut/contains-x-back? n xs))
                 (sut/x-back= n xs next-x))))))

   (testing "with p=0,"
     (let [p      0.0
           xs     (take xs-length (sut/->xs candidates n p))
           next-x (sut/->next-x candidates n p xs)]
       (testing "either xs is not long enough yet, or next-x is not-= to x-back"
         (is (or (not (sut/contains-x-back? n xs))
                 (not (sut/x-back= n xs next-x)))))))))
